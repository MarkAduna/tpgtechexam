var pageUrl = Cypress.env('pageUrl')

describe('Validate Landing page',() => {
    it('Landing page is loading',() => {
        cy.on('uncaught:exception', () => false)
        cy.visit('/')
          .url().should('contain', pageUrl)
          .title().should('contain', 'SIM Only and BYO Mobile Phone Plans | Vodafone Australia')
          .get('[data-testid="title"]').should('contain.text', 'SIM Only plans when you BYO phone')
    })
})