var endpoint = Cypress.env('apiUrl')

describe('Validate API endpoint', () => {
    it('Check if the API endpoint is available', () => {
        cy.on('uncaught:exception', () => false)
        cy.request(endpoint)
          .then(response => {
            expect(response.status).to.eq(207)
            expect(response.body).to.not.be.null
          })
        })
})