const plansList = Cypress.env('plansList')
const plans = plansList.planListing.plans
Cypress.config('defaultCommandTimeout', 10000);

describe('Validate Cart Test', () => {
    before(() => {
      console.log(plans)
      expect(plans).to.be.an('array')
    })

    plans.forEach((plan) => {
      var planProductName = plan.planProductName
      var planId = plan.id
      var price = plan.recurringCharge

      it('Check Cart and price for plan ' + planProductName, () => {
        cy.on('uncaught:exception', () => false)
        cy.visit('/')
          .get('[id="carousel-block-view-primaryPlans"]').within(() => {
            cy.get('[data-testid="plan-card-' + planId + '"]')
              .within(() => {
                cy.get('[data-testid="plan-card-title-' + planId + '"]')
                  .click()
                cy.get('[data-testid="plan-card-actions-mobile"]')
                  .within(() => {
                    cy.get('[data-testid="select-plan-cta"]')
                      .click()

                // page 2
                cy.title()
                  .should('contain', 'Extras | Vodafone Australia')
                cy.get('[data-testid=sticky-cart-container]',{withinSubject:null})
                  .find('h5')
                  .should('contain.text', "You'll pay $" + price + ".00 per month")
                cy.get('[data-testid=sticky-cart-title-cta-button]',{withinSubject:null})
                  .click()

                // page 3
                cy.title()
                  .should('contain', 'Cart | Vodafone Australia')
                cy.get('[data-testid="cart-card-title-button"]',{withinSubject:null})
                  .find('h4')
                  .should('contain.text', planProductName)

                cy.get('[data-testid=remove-bundle-cta]',{withinSubject:null})
                  .click()
                })
                })
            })
        })
    })
})
