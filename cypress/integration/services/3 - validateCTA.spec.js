
const plansList = Cypress.env('plansList')
const plans = plansList.planListing.plans

describe('Validate CTA Test', () => {
  before(() => {
    console.log(plans)
    expect(plans).to.be.an('array')
  })

  plans.forEach((plan) => {
    var planProductName = plan.planProductName
    var planId = plan.id
    var ctaLabel = plan.ctaLabel

    it('Check CTA for plan ' + planProductName, () => {
      cy.on('uncaught:exception', () => false)
      cy.visit('/')

      cy.get('[data-testid="plan-card-' + planId + '"]').within(() => {
        cy.get('[data-testid="plan-card-actions-desktop"]').within(() => {
          cy.get('[data-testid="select-plan-cta"]').should('contain.text', ctaLabel)
        })
        cy.get('[data-testid="plan-card-actions-mobile"]').within(() => {
          cy.get('[data-testid="select-plan-cta"]').should('contain.text', ctaLabel)
        })
      })
    })
  })
})
