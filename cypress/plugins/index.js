/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)
const got = require('got')
/**
 * @type {Cypress.PluginConfig}
 */
// eslint-disable-next-line no-unused-vars
module.exports = async (on, config) => {

  const response = await got('https://api-prod.prod.cms.df.services.vodafone.com.au/plan/postpaid-simo?serviceType=New').json()

  config.baseUrl = 'https://www.vodafone.com.au/plans/sim-only'
  config.env.pageUrl = 'https://www.vodafone.com.au/plans/sim-only'
  config.env.apiUrl = 'https://api-prod.prod.cms.df.services.vodafone.com.au/plan/postpaid-simo?serviceType=New'
  config.env.plansList = response
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config
  return config
}
