ifdef DOTENV
	DOTENV_TARGET=dotenv
else
	DOTENV_TARGET=.env
endif

## Entry Points ##

deps:
	npm install

test:
	npm run cypress-run --headless --browser chrome
