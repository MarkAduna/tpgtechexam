# tpgtechexam

## Description
This suite runs the following tests:
1. Visit Vodafone's online Postpaid SIM only page https://www.vodafone.com.au/plans/sim-only
2. Send a GET request to https://api-prod.prod.cms.df.services.vodafone.com.au/plan/postpaid-simo?serviceType=New
3. Validate if for all plans cta ('Add to cart' button) in the UI matches with the ctaLabel field (under planListing/plans/)from the above GET request in Step 2.
4. Select any plan and validate if the price shown in sticky cart is same as the plan selected.
5. Proceed to cart and validate if cart page is shown with the correct product.

## How to run
1. Ensure you have node js and npm installed
2. This suite is made easier to run by using Make as a runner

To run the test:

1. run >make deps - this will install dependencies
2. run >make test - this will run the test

# Build Pipeline
A pipeline is available in GitLab to see a CI/CD run - https://gitlab.com/MarkAduna/tpgtechexam/-/pipelines/392769453/builds
please let me know if you need access

A Github Actions pipeline is also available - https://github.com/MarkAduna/tpgtechexam/actions
